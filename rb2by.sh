#!/bin/bash

cp red_squares_to_blue_dots.svg blue_squares_to_yellow_dots.svg

b=100
tor=200
tog=200
tob=0

for (( r=0; r<=200; r=r+8 ))
do
  #for (( b=100; b>=0; b=b-4))
  #do


  rhex=$(echo "obase=16; $r" | bc | tr '[:upper:]' '[:lower:]')
  ghex="00"
  bhex=$(echo "obase=16; $b" | bc | tr '[:upper:]' '[:lower:]')

  if [ ${#rhex} = 1 ]; then rhex="0$rhex"; fi
  if [ ${#ghex} = 1 ]; then ghex="0$ghex"; fi
  if [ ${#bhex} = 1 ]; then bhex="0$bhex"; fi

  torhex=$(echo "obase=16; $tor" | bc | tr '[:upper:]' '[:lower:]')
  toghex=$(echo "obase=16; $tog" | bc | tr '[:upper:]' '[:lower:]')
  tobhex=$(echo "obase=16; $tob" | bc | tr '[:upper:]' '[:lower:]')

  if [ ${#torhex} = 1 ]; then torhex="0$torhex"; fi
  if [ ${#toghex} = 1 ]; then toghex="0$toghex"; fi
  if [ ${#tobhex} = 1 ]; then tobhex="0$tobhex"; fi

  #cho "sed -i 's/$rhex$ghex$bhex/$torhex$toghex$tobhex/g' blue_squares_to_yellow_dots.svg"
  #exit 1

  sed -i "s/$rhex$ghex$bhex/$torhex$toghex$tobhex/g" blue_squares_to_yellow_dots.svg

  b=$((b-4))
  tor=$((tor-8))
  tog=$((tog-4))
  tob=$((tob+8))

  #exit 1

  #done
done

for (( r=209; r<=254; r=r+9 ))
do
  rhex=$(echo "obase=16; $r" | bc | tr '[:upper:]' '[:lower:]')
  ghex="00"
  bhex="00"

  if [ ${#rhex} = 1 ]; then rhex="0$rhex"; fi
  if [ ${#ghex} = 1 ]; then ghex="0$ghex"; fi
  if [ ${#bhex} = 1 ]; then bhex="0$bhex"; fi

  torhex="$ghex";
  toghex=$(echo "obase=16; 100" | bc | tr '[:upper:]' '[:lower:]')
  tobhex="$rhex";

  if [ ${#torhex} = 1 ]; then torhex="0$torhex"; fi
  if [ ${#toghex} = 1 ]; then toghex="0$toghex"; fi
  if [ ${#tobhex} = 1 ]; then tobhex="0$tobhex"; fi
    
  sed -i "s/$rhex$ghex$bhex/$torhex$toghex$tobhex/g" blue_squares_to_yellow_dots.svg
done
