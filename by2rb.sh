#!/bin/bash

cp blue_squares_to_yellow_dots.svg red_squares_to_blue_dots.svg
r=200
g=200
tor=0
tog=0
tob=100

for (( b=0; b<=200; b=b+8 ))
do
  #for (( b=100; b>=0; b=b-4))
  #do


  rhex=$(echo "obase=16; $r" | bc | tr '[:upper:]' '[:lower:]')
  ghex=$(echo "obase=16; $g" | bc | tr '[:upper:]' '[:lower:]')
  bhex=$(echo "obase=16; $b" | bc | tr '[:upper:]' '[:lower:]')

  if [ ${#rhex} = 1 ]; then rhex="0$rhex"; fi
  if [ ${#ghex} = 1 ]; then ghex="0$ghex"; fi
  if [ ${#bhex} = 1 ]; then bhex="0$bhex"; fi

  torhex=$(echo "obase=16; $tor" | bc | tr '[:upper:]' '[:lower:]')
  toghex=$(echo "obase=16; $tog" | bc | tr '[:upper:]' '[:lower:]')
  tobhex=$(echo "obase=16; $tob" | bc | tr '[:upper:]' '[:lower:]')

  if [ ${#torhex} = 1 ]; then torhex="0$torhex"; fi
  if [ ${#toghex} = 1 ]; then toghex="0$toghex"; fi
  if [ ${#tobhex} = 1 ]; then tobhex="0$tobhex"; fi

  #cho "sed -i 's/$rhex$ghex$bhex/$torhex$toghex$tobhex/g' blue_squares_to_yellow_dots.svg"
  #exit 1

  sed -i "s/$rhex$ghex$bhex/$torhex$toghex$tobhex/g" red_squares_to_blue_dots.svg

  r=$((r-8))
  g=$((g-4))

  tor=$((tor+8))
  tob=$((tob-4))

  #exit 1

  #done
done

for (( b=209; b<=254; b=b+9 ))
do
  rhex="00"
  ghex=$(echo "obase=16; 100" | bc | tr '[:upper:]' '[:lower:]')
  bhex=$(echo "obase=16; $b" | bc | tr '[:upper:]' '[:lower:]')

  if [ ${#rhex} = 1 ]; then rhex="0$rhex"; fi
  if [ ${#ghex} = 1 ]; then ghex="0$ghex"; fi
  if [ ${#bhex} = 1 ]; then bhex="0$bhex"; fi

  torhex="$bhex";
  toghex="00";
  tobhex="00";

  if [ ${#torhex} = 1 ]; then torhex="0$torhex"; fi
  if [ ${#toghex} = 1 ]; then toghex="0$toghex"; fi
  if [ ${#tobhex} = 1 ]; then tobhex="0$tobhex"; fi
    
  sed -i "s/$rhex$ghex$bhex/$torhex$toghex$tobhex/g" red_squares_to_blue_dots.svg
done
